document.addEventListener('DOMContentLoaded', function() {
    updateClock(); // Mise à jour de l'horloge dès le chargement de la page
    setInterval(updateClock, 1000); // Mise à jour de l'horloge chaque seconde
});

function updateClock() {
    const now = new Date();
    const hours = now.getHours().toString().padStart(2, '0');
    const minutes = now.getMinutes().toString().padStart(2, '0');
    const seconds = now.getSeconds().toString().padStart(2, '0');
    document.getElementById('clock').textContent = `${hours}:${minutes}:${seconds}`;
}

// Chronomètre
let chronometerInterval;
let chronometerRunning = false;
let milliseconds = 0;

function startStopChronometer() {
    if (chronometerRunning) {
        clearInterval(chronometerInterval);
        document.getElementById('startStop').textContent = 'Start';
    } else {
        chronometerInterval = setInterval(updateChronometer, 10);
        document.getElementById('startStop').textContent = 'Stop';
    }
    chronometerRunning = !chronometerRunning;
}

function updateChronometer() {
    milliseconds += 10;
    const minutes = Math.floor(milliseconds / (60 * 1000)).toString().padStart(2, '0');
    const seconds = Math.floor((milliseconds % (60 * 1000)) / 1000).toString().padStart(2, '0');
    const remainingMilliseconds = (milliseconds % 1000).toString().padStart(3, '0');
    document.getElementById('minutes').textContent = minutes;
    document.getElementById('seconds').textContent = seconds;
    document.getElementById('milliseconds').textContent = remainingMilliseconds;
}

function resetChronometer() {
    clearInterval(chronometerInterval);
    chronometerRunning = false;
    milliseconds = 0;
    document.getElementById('minutes').textContent = '00';
    document.getElementById('seconds').textContent = '00';
    document.getElementById('milliseconds').textContent = '000';
    document.getElementById('startStop').textContent = 'Start';
}

// Minuteur
let timerInterval;
let timerRunning = false;
let remainingTime = 0;

function startStopTimer() {
    if (timerRunning) {
        clearInterval(timerInterval);
        document.getElementById('startStopTimer').textContent = 'Start Timer';
    } else {
        const timerInput = document.getElementById('timerInput').value;
        const [hours, minutes, seconds] = timerInput.split(':').map(Number);

        if (!isNaN(hours) && !isNaN(minutes) && !isNaN(seconds)) {
            remainingTime = (hours * 60 * 60 + minutes * 60 + seconds) * 1000;
            timerInterval = setInterval(updateTimer, 10);
            document.getElementById('startStopTimer').textContent = 'Stop Timer';
        }
    }
    timerRunning = !timerRunning;
}

function updateTimer() {
    remainingTime -= 10;
    if (remainingTime < 0) {
        clearInterval(timerInterval);
        timerRunning = false;
        document.getElementById('timerInput').value = '';
        document.getElementById('startStopTimer').textContent = 'Start Timer';
        alert('Timer Expired!');
    } else {
        const hours = Math.floor(remainingTime / (60 * 60 * 1000)).toString().padStart(2, '0');
        const minutes = Math.floor((remainingTime % (60 * 60 * 1000)) / (60 * 1000)).toString().padStart(2, '0');
        const seconds = Math.floor((remainingTime % (60 * 1000)) / 1000).toString().padStart(2, '0');
        const remainingMilliseconds = (remainingTime % 1000).toString().padStart(3, '0');
        document.getElementById('timerDisplay').textContent = `${hours}:${minutes}:${seconds}:${remainingMilliseconds}`;
    }
}

function resetTimer() {
    clearInterval(timerInterval);
    timerRunning = false;
    remainingTime = 0;
    document.getElementById('timerInput').value = '';
    document.getElementById('timerDisplay').textContent = '00:00:000';
    document.getElementById('startStopTimer').textContent = 'Start Timer';
}
